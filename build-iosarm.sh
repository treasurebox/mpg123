
PLATFORM=iPhoneOS
ARCHS=armv7
TOOLCHAIN=iphoneos
export EXTRAFLAGS="-miphoneos-version-min=5.1"


#PLATFORM=iPhoneSimulator
#ARCHS=i386
#TOOLCHAIN=iphonesimulator
#export EXTRAFLAGS="-mios-simulator-version-min=5.1"

SDKVERSION=7.0

CURRENTPATH=`pwd`
mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"


mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib"

#ARCHS='i386 arm'

#ARCHS=arm
DEVELOPER=`xcode-select -print-path`

export CROSS_BASE="${DEVELOPER}"
export CROSS_TOP="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
export CROSS_SDK="${PLATFORM}${SDKVERSION}.sdk"

for ARCH in $ARCHS 
do
	make distclean
	
	echo "Building mpg123 ${VERSION} for ${PLATFORM} ${SDKVERSION} ${ARCH}"
	echo "Please stand by... (compiler at $CROSS_TOP)"

	export CPP="xcrun --sdk $TOOLCHAIN cc -arch armv7 -E"
	export CC="xcrun --sdk $TOOLCHAIN cc -arch armv7"
	#export CC="${CROSS_BASE}/usr/bin/gcc -arch armv7"
	export AS="xcrun --sdk $TOOLCHAIN as"
	export RANLIB="xcrun --sdk $TOOLCHAIN ranlib"
	export AR="xcrun --sdk $TOOLCHAIN ar"
	#export LD="${CROSS_BASE}/usr/bin/ld -arch armv7"
	export LD="xcrun --sdk $TOOLCHAIN ld -arch armv7"
	export NM="xcrun --sdk $TOOLCHAIN nm"  
	#no libtool!
	export LIBTOOL="xcrun --sdk $TOOLCHAIN libtool"

	export SYSROOT="${CROSS_TOP}/SDKs/${CROSS_SDK}"
	echo "system root: $SYSROOT"
	

	export CFLAGS="-arch $ARCH -isysroot $SYSROOT $EXTRAFLAGS"
	export LDFLAGS="-Xlinker -ios_version_min -Xlinker 5.1 -Xlinker -syslibroot -Xlinker $SYSROOT -Xlinker -lc"

	mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
	LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-openssl-${VERSION}.log"


	#echo configure for cross compiling...
	#modules are disabled otherwise libltdl would be needed on target 
	./configure --enable-shared=no --enable-static=yes --enable-modules=no --host=$ARCH-apple-darwin --prefix=${CURRENTPATH}/lib/$ARCH 
	#--host=$ARCH-linux-gnueabi 

	
	echo start build 
	make && make install
done
